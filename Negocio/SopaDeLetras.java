package Negocio;

/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    //matriz
    private char sopas[][];

    /**
     * Constructor default
     */
    public SopaDeLetras()
    {

    }

    /**
     * Constructor con parámetros
     */
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras == null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabrasSinEspacios = palabras.replace(" ","");
        String palabras2[] = palabrasSinEspacios.split(",");        
        this.sopas = new char[palabras2.length][];

        int i=0;
        for(String palabraX:palabras2)
        {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX,this.sopas[i]);
            i++;
        }
    }

    private void pasar (String palabra, char fila[])
    {
        for(int j=0;j < palabra.length();j++)
        {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString()
    {
        String msg = "";

        for(int i=0;i<this.sopas.length;i++)
        {
            for (int j=0;j<this.sopas[i].length;j++)
            {
                msg = msg + this.sopas[i][j] + "\t";
            }
            msg = msg + "\n";
        }
        return msg;
    }

    public String toString2()
    {
        String msg = "";
        for(char filas[]:this.sopas)
        {
            for (char dato : filas)
            {
                msg = msg + dato + "\t";
            }
            msg = msg + "\n";
        }
        return msg;
    }

    public boolean esCuadrada()
    {
        boolean esCuadrada = true;
        for(int i = 0; i<sopas.length && esCuadrada==true;i++)
        {
            esCuadrada = sopas.length == sopas[i].length; 
        }      
        return esCuadrada;
    }

    public boolean esDispersa()
    {
        boolean esDispersa = false;
        int cantidadColumnasFila0 = this.sopas[0].length;

        for(int i = 1; i<sopas.length && esDispersa==false;i++)
        {
            esDispersa = cantidadColumnasFila0 != this.sopas[i].length;
        }        
        return esDispersa;
    }

    public boolean esRectangular()
    {
        boolean esRectangular = false;        
        esRectangular = esCuadrada() == false && esDispersa() == false;      
        return esRectangular;
    }

    public int getContar(String palabra)
    { 
        int contador=0;
        int k=0;
        String palabrasSinEspacios = palabra.replace(" ","");
        char buscar[]= new char[palabrasSinEspacios.length()];
        
        for(int i=0;i<palabrasSinEspacios.length();i++)
        {
            buscar[i]=palabrasSinEspacios.charAt(i);
        }

        for(int i=0;i<sopas.length;i++)
        {        
            for(int j=0;j<sopas[i].length;j++){
                if(sopas[i][j]==buscar[k]){
                    k++;
                }else{
                    k = 0;
                    if(sopas[i][j]==buscar[k]){
                        k++;
                    }                              
                }
                if(k==buscar.length){
                    contador++; 
                    k=0;
                }  
            }  
            k=0;
        }
        return contador;
    }

    /*
    debe ser cuadrada sopas
     */
    public char [] getDiagonalPrincipal() throws Exception
    {
        if(esCuadrada() == false)
        {
            throw new Exception ("la matriz no es cuadra. por lo tanto, no tiene diagonal principal");
        }
        //comenzar a sacar la diagonal         
        char diagonal [] = new char [sopas.length];

        for(int i = 0; i<diagonal.length;i++)
        {
            diagonal[i] = sopas[i][i];
        }

        return  diagonal;       
    }

    //Start GetterSetterExtension Code
    /**Getter method sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Code
//!
}
